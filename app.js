var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// MongoDB
var mongoose = require('mongoose');
mongoose.connect('mongodb://database_user:database_password@localhost/pollsdb');
//mongoose.connect('mongodb://localhost/pollsdb');




var app = express();



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// Configuring Passport
var passport = require('passport');
var expressSession = require('express-session');
app.use(expressSession({secret: 'mySecretKey'}));
app.use(passport.initialize());
app.use(passport.session());

// Initialize Passport
var initPassport = require('./src/passport/init');
initPassport(passport);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


var routes = require('./routes/index')(passport);
var users = require('./routes/users');

app.use('/', routes);
app.use('/users', users);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.use(errorHandler);
function errorHandler(err, req, res, next) {
    res.status(500);
    res.render('error', { error: err });
}


var server = require('http').createServer(app);
var io = require('socket.io')(server);
//io = require('socket.io').listen(app,{});

server.listen(80, function () {
   // console.log('Example app listening on port 3000!');
});

app.set('socketio', io);

//io.sockets.on('connection', function(socket) {
//    socket.volatile.emit('notification', "New question");
//});
module.exports = app;
