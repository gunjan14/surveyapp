//var mongoose = require('mongoose');
var restful = require('node-restful');
var mongoose = restful.mongoose;
var AutoIncrement = require('mongoose-sequence');

// Subdocument schema for votes
var voteSchema = new mongoose.Schema({ ip: 'String' });

// Subdocument schema for poll choices
var choiceSchema = new mongoose.Schema({
    text: String,
    isCorrect:Boolean,
    votes: [voteSchema]
});

// Document schema for polls
var PollSchema = new mongoose.Schema({
   _id: Number,
    question: { type: String, required: true },
    visible:{type:Boolean,default:true},
    choices: [choiceSchema]
},{ _id: false });
PollSchema.plugin(AutoIncrement);

// Return model
module.exports = restful.model('Poll', PollSchema);
